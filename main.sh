#!/bin/bash
#git practice

#function to explain the program
about() {
  echo "My magic prompt is a tool to do some functions like sending a mail, showing some informations etc. \n Try the help command to know more informations";
}

#function to check the age 
age() {
  echo -n 'How old are you ? ';
  read myAge
  reg='^[0-9]+$'
if ! [[ ${myAge} =~ ${reg} ]] ; then
   echo "error: Not a number" >&2; exit 1
elif [ "$myAge" -gt 17 ] ;
  then
  echo 'You are adult';
  else 
  echo 'You are minor';
  fi
}

#function to delete a file
deleteFile() {
  echo -n 'which file do you want to delete ? '
  read deleteFile;
  if [ -f "$deleteFile" ]; then
  rm ${deleteFile};
  else 
  echo 'Your file does not exist';
  fi
}

#function to delete a directory
deleteFolder() {
  echo -n 'which directory do you want to delete ? '
  read deleteFolder;
  if [ "$deleteFolder" == "*" ] || [ "$deleteFolder" == " " ]; then
  echo "Are you crazy ? "
  elif [ -d "$deleteFolder" ]; then
  rmdir "${deleteFolder}";
  else 
  echo 'Your directory does not exist'
  fi
}

#function to send email
email() {
  echo -n 'Who do you want to send an Email ? '
  read contact
  echo -n 'Write the object '
  read object
  echo -n 'Write your mail content '
  read mailContent

  echo "${mailContent}" | mail -s "${object}" $contact
}

#Show all commands
help() {
  echo "ls : To show some informations about the current directory"
  echo "rm : To deletes a file"
  echo "rmd | rmdir : To delete a directory"
  echo "about : A description of your program"
  echo "version | vers | -v : To display the current version"
  echo "age : To show your age and announces if you are of age or not"
  echo "quit : To leave the program"
  echo "profile : To show some informations about yourself"
  echo "passw : To modify your password"
  echo "cd : eave the program"
  echo "pwd : To show current directory"
  echo "hour : To display the time"
  echo "httpget : To download source code of a page and save it in a named file"
  echo "smtp : To send an email"
  echo "open : To open a file with VIM editor"
}

#hour 
hour() {
echo $(date +"%T")
}

#function to download source code
httpget() {
  echo -n 'Which source code do you want to download ? '
  read urlPage;
  echo -n 'Name file : ';
  read namePage;
  wget $urlPage -O $namePage
}

#function to list 
list() {
  ls;
}

#changedirectory
navigation() {
  echo -n 'Where do you want to go ? '
  read path;
  folder=$path
  if [ -d "$folder" ]; then
    cd $folder;
  else 
    echo "$folder is not a directory."
  fi
}

open() {
echo -n 'Name file : ';
read fileName;
FILE=$fileName;
vim $FILE;

}

#Change password (not finished)
password() {
  echo -n "New password : "
  read newPassword;
  echo -n 'Password confirm : '
  read passwordConfirm;

  if [ $newPassword != $passwordConfirm  ]
  then 
  echo 'Passwords are not equals'
  fi
}

#function to show profile informations
profile() {
  profile=("Charles" "Lafont" "22 years old" "charleslfnt@gmail.com")
  echo ${profile[*]}
}

#function to quit the program
quit() {
  echo 'See you next time :)'
  exit;main
}

#function to show the current version
version() {
echo "You are on the 1.0 version"
}

#stop functions 

#commands 
command() {
  while [ 1 ]; do
  echo -n 'Type a command : '
 
  read command
  
  case "$command" in
  
  about ) about;;

  age ) age;;

  rm ) deleteFile;;

  rmd | rmdir ) deleteFolder;;

  smtp ) email;;

  help ) help;;

  hour ) hour;;

  httpget ) httpget;;

  ls ) list;;

  cd ) navigation;;

  open ) open;;

  passw ) password;;

  profile ) profile;;

  pwd ) pwd;;

  version | -v | vers ) version;;

  quit ) quit;;

#Command error
    * ) echo "This command does not exist :( try again ";;
esac
done
}

main() {
#Login
echo -n 'What is your name ? ';
  read name
echo 'Your password '
  read -s password

#logs verification
if [ ${name} != 'Charles' ] || [ ${password} != 'password' ] 
then 
  echo 'Error connection, try again';
  main;
fi

command

exit
}
main